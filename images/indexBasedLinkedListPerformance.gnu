set title "Index-Based Linked List Iteration" font ",20"
set key left box
set samples 50
set style data points
set xlabel "List Size (k)"
set ylabel "Time (sec)"

#set terminal svg
#set output "graph.svg"
set terminal pdf
set output "graph.pdf"

#set size .75,.75

plot [0:1000] 0.00064101*x**2 -0.11168*x + 9.7822 lt rgb "red" title "Regression", \
     'points.dat' lt rgb "blue" with linespoints title "Samples"

     #linear regression: essentially flat
     #5.138345865E-5*x + 0.0037736

