%!TEX root = ComputerScienceTwo.tex

\newglossaryentry{algorithm}
{
  name=algorithm,
  description={a process or method that consists of a specified step-by-step set of operations}
}

\newglossaryentry{corner case}
{
  name=corner case,
  description={a scenario that occurs outside of typical operating parameters or situations; an exceptional case or situation that may need to be dealt with in a unique or different manner}
}

\newglossaryentry{data anomaly}
{
  name=data anomaly,
  plural={data anomalies},
  description={Redundant or inconsistent data in a database that violates the intended integrity of the data}
}

\newglossaryentry{dependency inversion}
{
  name=dependency inversion,
  description={The decoupling of software modules by defining an interface between them.
  Instead of one module depending directly on the other, both end up depending on the interface. One module \emph{implements} the interface and the other (called a \emph{client}) \emph{uses} or consumes the interface.  This allows client code to not have to depend on a particular implementation.  Different implementations can be swapped out and changed with minimal code changes in the client code}
}

\newglossaryentry{duck typing}
{
  name=duck typing,
  description={In dynamically typed languages an object may not have a declared type and so
  to determine its type you rely on what method(s) and/or member variable(s) it has; ``if
  it walks like a duck and talks like a duck'' then it is a duck.  That is, if it has the
  methods and variables that you expect of a particular type, then for all intents and
  purposes it \emph{is} that type}
}

\newglossaryentry{flat file}
{
  name=flat file,
  description={A manner in which data is stored typically in a single 
  file where the data model is ``flattened'' into a single table with many 
  columns and each row representing a record}
}

\newglossaryentry{idiom}
{
  name=idiom,
  description={in the context of software an idiom is a common code or design pattern}
}

\newglossaryentry{interface segregation principle}
{
  name=interface segregation,
  description={The principle that no client should be forced to depend on methods or functionality it does not use.  This principle essentially states that interfaces
  should be very minimal in their design and that many small interfaces should be
  combined to create more complex behavior}
}

\newglossaryentry{iterator}
{
  name=iterator,
  description={a pattern that allows a user to more easily and conveniently iterate over the elements in a collection}
}

\newglossaryentry{leaky abstraction}
{
  name=leaky abstraction,
  description={a design that exposes details and limitations of an implementation that should otherwise be hidden through encapsulation}
}

\newglossaryentry{Liskov substitution principle}
{
  name=Liskov substitution principle,
  description={The principle that if $T$ is a subtype of $S$ then any instance of $S$ can be replaced with any subtype $T$ without breaking a program}
}

\newglossaryentry{mixin}
{
  name=mixin,
  description={A mixin is a class that contains methods that other classes may use without
  a direct inheritance relationship.  The functionality is ``mixed in'' to the class}
}

\newglossaryentry{normalization}
{
  name=normalization,
  description={the process of restructuring data and tables in a database, separating related
  data into their own tables in order to reduce redundancy and minimize the potential for 
  data anomalies}
}

\newglossaryentry{open/closed principle}
{
  name=open/closed principle,
  description={The principle that ``software entities should be open for extension, but closed for modification.''  This generally refers to inheritance in OOP: that a class's functionality can be extended through a subclass, but that the class itself should not be modifiable (so that other classes that depend on it don't have the rug pulled out from under them)}
}

\newglossaryentry{parameterized polymorphism}
{
  name=parameterized polymorphism,
  description={a means in which you can make a piece of code (a method, class, or variable) generic so that its type can vary depending on the context in which the code is used}
}

\newglossaryentry{polymorphism}
{
  name=polymorphism,
  description={A mechanism by which a piece of code (class, method, variable) can be 
  written generically so that it can ``take on many forms'' or used on different
  types in different places of a program}
}

\newglossaryentry{query}
{
  name=query,
  plural={queries},
  description={A request for data or an operation on data in a database}
}

\newglossaryentry{queue}
{
  name=queue,
  description={a collection data structure that holds elements in a first-in first-out manner}
}

\newglossaryentry{random access}
{
  name=random access,
  description={a mechanism by which elements in an array can be accessed by simply computing a memory offset of each element relative to the beginning of the array}
}

\newglossaryentry{serialization}
{
  name=serialization,
  description={translation of data into alternative formats, usually to plaintext data interchange formats such as JSON or XML}
}

\newglossaryentry{single responsibility principle}
{
  name=single responsibility principle,
  description={a general guideline that every module, class, function, etc. in code should have only a single responsibility or represent a single entity.  More complex code can be written by composing these together}
}

\newglossaryentry{stack}
{
  name=stack,
  description={a collection data structure that holds elements in a last-in first-out manner}
}

\newglossaryentry{syntactic sugar}
{
  name=syntactic sugar,
  description={syntax in a language or program that is not absolutely necessary 
    (that is, the same thing can be achieved using other syntax), but may be 
    shorter, more convenient, or easier to read/write.  In general, such syntax 
    makes the language ``sweeter'' for the humans reading and writing it}
}

\newglossaryentry{transaction}
{
  name=transaction,
  description={The basic unit of work in a database that is treated as an atomic or an all-or-nothing event.  A transaction may consist of one or more queries}
}

\newglossaryentry{tuple}
{
  name=tuple,
  description={An ordered sequence of elements.  Typically the notation $(x_1, x_2, \ldots, x_n)$ 
  is used.  Tuples correspond to rows or records in a database}
}


\newacronym{acidLabel}{ACID}{Atomicity, Concurrency, Isolation, Durability}

\newacronym{adtLabel}{ADT}{Abstract Data Type}

\newacronym{apiLabel}{API}{Application Programmer Interface}

\newacronym{astLabel}{AST}{Abstract Syntax Tree}

\newacronym{bfsLabel}{BFS}{Breadth First Search}

\newacronym{blobLabel}{BLOB}{Binary Large Object}

\newacronym{bstLabel}{BST}{Binary Search Tree}

\newacronym{crudLabel}{CRUD}{Create Retrieve Update Destroy}

\newacronym{csvLabel}{CSV}{Comma-Separated Value}

\newacronym{dagLabel}{DAG}{Directed Acyclic Graph}

\newacronym{dbaLabel}{DBA}{Database Administrator}

\newacronym{ddlLabel}{DDL}{Document Description Language}

\newacronym{dfsLabel}{DFS}{Depth First Search}

\newacronym{dipLabel}{DIP}{Dependency Inversion Principle}

\newacronym{dryLabel}{DRY}{Don't Repeat Yourself}

\newacronym{ediLabel}{EDI}{Electronic Data Interchange}

\newacronym{erLabel}{ER}{Entity Relation}

\newacronym{fifoLabel}{FIFO}{First-In First-Out}

\newacronym{fkLabel}{FK}{Foreign Key}

\newacronym{fossLabel}{FOSS}{Free and Open Source Software}

\newacronym{graspLabel}{GRASP}{General Responsibility Assignment Software Patterns}

\newacronym{ispLabel}{ISP}{Interface Segregation Principle}

\newacronym{jdbcLabel}{JDBC}{Java Database Connectivity API}

\newacronym{jpaLabel}{JPA}{Java Persistence API}

\newacronym{jsonLabel}{JSON}{JavaScript Object Notation}

\newacronym{jvmLabel}{JVM}{Java Virtual Machine}

\newacronym{kvpLabel}{KVP}{Key-Value Pair}

\newacronym{lifoLabel}{LIFO}{Last-In First-Out}

\newacronym{lspLabel}{LSP}{Liskov Substitution Principle}

\newacronym{ocpLabel}{OCP}{Open-Closed Principle}

\newacronym{ormLabel}{ORM}{Object-Relational Mapping}

\newacronym{pkLabel}{PK}{Primary Key}

\newacronym{pojoLabel}{POJO}{Plain-Old Java Object}

\newacronym{raiiLabel}{RAII}{Resource Acquisition Is Initialization}

\newacronym{rdmsLabel}{RDMS}{Relational Database Management System}

\newacronym{rtmLabel}{RTM}{Read The Manual}

\newacronym{solidLabel}{SOLID}{SOLID Principles (Single Responsibility, Open/Closed, Liskov Substitution, Interface Segregation, Dependency Inversion}

\newacronym{sqlLabel}{SQL}{Structured Query Language}

\newacronym{srpLabel}{SRP}{Single Responsibility Principle}

\newacronym{xmlLabel}{XML}{Extensible Markup Language}

\newacronym{yagniLabel}{YAGNI}{You Ain't Gonna Need It}


