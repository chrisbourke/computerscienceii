\documentclass{article}
\usepackage{fullpage}
\usepackage{tikz}
\usepackage{algorithm2e}
\begin{document}

A \emph{perfect} binary tree is a tree in which every internal node 
has exactly two children and all the leaves are at the same level.
This means that the tree is full at every level up to its deepest
level, $d$ (the depth).  

A perfect binary search tree contains keys $1, 2, \ldots, 2^{d+1} - 1$ 
organized into a perfect tree structure.  An example for of a 
perfect binary search tree of depth $d = 4$ is given in Figure 
\ref{figure:perfectBinarySearchTreeDepthFour}.


\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.75,
transform shape,
level distance=1.25cm,
level 1/.style={sibling distance=80mm},
level 2/.style={sibling distance=40mm},
level 3/.style={sibling distance=20mm},
level 4/.style={sibling distance=10mm},
every node/.style={circle,draw,minimum size=.75cm}]
\node{$16$}
child {node {$8$} 
child {node {$4$} 
child {node {$2$} 
child {node {$1$} 
child[draw opacity=0.0] {}
child[draw opacity=0.0] {}
}
child {node {$3$} 
child[draw opacity=0.0] {}
}
}
child {node {$6$} 
child {node {$5$}}
child {node {$7$} 
child[draw opacity=0.0] {}
child[draw opacity=0.0] {}
}
}
}
child {node {$12$} 
child {node {$10$}
  child {node {$9$}}
  child {node {$11$}}  
}
child {node {$14$} 
child {node {$13$}}
child {node {$15$}}
}
}
}
child {node {$24$} 
child {node {$20$}
  child {node {$18$}
    child {node {$17$}} 
    child {node {$19$}}
  } 
  child {node {$22$}
    child {node {$21$}} 
    child {node {$23$}}
  }
}
child {node {$28$} 
child {node {$26$} 
child {node {$25$}}
child {node {$27$} 
}
}
child {node {$30$} 
  child {node {$29$}}
  child {node {$31$}}
}
}
}
;
\end{tikzpicture}
\caption{A perfect binary search tree of depth $d = 4$ with $n = 2^{d+1} - 1 = 31$ sequential keys.}
\label{figure:perfectBinarySearchTreeDepthFour}
\end{figure}

Consider a very large perfect binary search tree, so large that it 
would be infeasible to construct (say $d = 100$).  Can we still work
``locally'' with the tree without explicitly building it?  Devise either
formulas or algorithms to determine the following.
\begin{itemize}
  \item Given a key value $k$, determine which level of the tree it lies in
  \item Given a key value $k$, determine its left child, right child and parent
  \item TODO: more?
\end{itemize}

Given $k$, to determine what level it lies in, we can make the following
observation: from the bottom up, the first key at each level follows
a geometric progression, $1, 2, 4, 8, \ldots, 2^d$.  Then, each key 
in that level is a multiple of the next power of two; from the bottom, 
\begin{itemize}
  \item $\mathrm{offset} = 2^0 = 1$: $\mathrm{offset} + j2^1$, $j = 0, 1, 2, \ldots, 2^{d-0} - 1$
  \item $\mathrm{offset} = 2^1 = 2$: $\mathrm{offset} + j2^2$, $j = 0, 1, 2, \ldots, 2^{d-1} - 1$
  \item $\mathrm{offset} = 2^2 = 4$: $\mathrm{offset} + j2^3$, $j = 0, 1, 2, \ldots, 2^{d-2} - 1$
  \item $\mathrm{offset} = 2^3 = 8$: $\mathrm{offset} + j2^4$, $j = 0, 1, 2, \ldots, 2^{d-3} - 1$
  \item $\mathrm{offset} = 2^i = 2^d$: $\mathrm{offset} + j2^{i+1}$, $j = 0, 1, 2, \ldots, 2^{d-d} - 1$
\end{itemize}
Because of this regularity, we can determine which level (or depth) a given
key is in by testing divisibility with the various offsets.  Since the 
depth $d = O(\log{n})$, this is efficient though not constant.

\begin{algorithm}
\For{$i=0, \ldots, d$}{
  $\mathrm{offset} \leftarrow 2^i$ \;
  \If{ $(k-\mathrm{offset}) \,\%\, 2^{i+1} = 0$ }{
    output $i$\;
  }  
}
\end{algorithm}

In a similar manner, we can determine if $k$ is a left child or 
right child of its parent by determining if $j$ is even or odd.
If $k$ is at depth $i$, then $k = 2^{i} + j2^{i+1}$; solving for
$j = \frac{k - 2^{i}}{2^{i+1}}$, if $j$ is even, then $k$ is a
left child, if $j$ is odd then it is a right child.

We can now use this information to determine the keys of the 
parent, left-child, and right-child.  Given $k$ and its depth
$i$, we know that:
\begin{itemize}
  \item The size of the subtree rooted at $k$ is of size $2^{i+1}-1$
  \item $k$ is the \emph{median} of the subtree by construction
  \item The minimum value is $k - (2^{i} - 1)$ and
  \item The maximum value is $k + (2^{i} - 1)$
  \item The left child and right child will be the medians 
  (same as average) of the subintervals, thus
  \item The left child is at $\lfloor \frac{2k - (2^i - 1)}{2} \rfloor$
  \item The right child is at $\lceil \frac{2k + (2^i - 1)}{2} \rceil$
  \item Finding the parent is even easier: it lies on the ``edge'' of the subtree, so
  \begin{itemize}
    \item If $k$ is a left-child its parent is maximum plus one
    \item If $k$ is a right child, its parent is minimum minus one
    \item needless to say is at level $i-1$ (one level up).
  \end{itemize}
\end{itemize}



\newpage


Now consider a perfect tree of depth $d$ constructed of key values 
$1, \ldots, n = 2^{d+1}-1$ such that a post-order traversal of the
tree results in a sorted order.  An example of such a tree with 
$d = 4$ is given in Figure \ref{figure:perfectBinaryGoogleTreeDepthFour}.

\begin{figure}[h]
\centering
\begin{tikzpicture}[scale=0.75,
transform shape,
level distance=1.25cm,
level 1/.style={sibling distance=80mm},
level 2/.style={sibling distance=40mm},
level 3/.style={sibling distance=20mm},
level 4/.style={sibling distance=10mm},
every node/.style={circle,draw,minimum size=.75cm}]
\node{$31$}
child {node {$15$} 
child {node {$7$} 
child {node {$3$} 
child {node {$1$} }
child {node {$2$} }
}
child {node {$6$} 
child {node {$4$}}
child {node {$5$} 
child[draw opacity=0.0] {}
child[draw opacity=0.0] {}
}
}
}
child {node {$14$} 
child {node {$10$}
  child {node {$8$}}
  child {node {$9$}}  
}
child {node {$13$} 
child {node {$11$}}
child {node {$12$}}
}
}
}
child {node {$30$} 
child {node {$22$}
  child {node {$18$}
    child {node {$16$}} 
    child {node {$17$}}
  } 
  child {node {$21$}
    child {node {$19$}} 
    child {node {$20$}}
  }
}
child {node {$29$} 
child {node {$25$} 
child {node {$23$}}
child {node {$24$} 
}
}
child {node {$28$} 
  child {node {$26$}}
  child {node {$27$}}
}
}
}
;
\end{tikzpicture}
\caption{A perfect post-order binary tree of depth $d = 4$ with keys constructed in a post-order manner.}
\label{figure:perfectBinaryGoogleTreeDepthFour}
\end{figure}



Once again, devise an algorithm or formula to determine the following:
\begin{itemize}
  \item Given a key value $k$, determine its left child, right child and parent
  \item Given a key value $k$, determine which level of the tree it lies in
\end{itemize}

Observations:
\begin{itemize}
  \item At the root is the value $n = 2^{d+1} - 1$
  \item Let $k$ be the given key at depth (or height?) $i$ (from the root, 
  $i = d, d-1, \ldots, 0$):
  \item There are $2^{i+1} - 1$ total nodes in the subtree rooted at $k$
  \item The overall range of the tree rooted at $k$ is
      $$k, k-1, k-2, \ldots, k-(2^{i+1}-1)$$
  \item With respect to the right-subtree:
  \begin{itemize}
    \item The root (right-child of $k$) is $k - 1$
    \item There are $2^i-1$ total nodes in it
    \item The range of values is 
      $$k-1, k-2, \ldots, k-(2^i-1)$$
  \end{itemize}
  \item With respect to the left-subtree:
  \begin{itemize}
    \item The root (left-child of $k$) is $k - (2^i - 1) - 1$
    \item There are $2^i-1$ total nodes in it
    \item The range of values is 
      $$k - (2^i - 1) - 1, \ldots, k-(2^{i+1}-2)$$
  \end{itemize}
\end{itemize}


\end{document}


